from django.db import models
from datetime import datetime
from user.models import User

# 吐槽和吐槽的评论都属于吐槽的这张表，吐槽的parent_id为None，评论则有parent_id



class Spit(models.Model):
    """吐槽模型类"""
    content = models.TextField(verbose_name='吐槽内容')
    publishtime = models.DateTimeField(default=datetime.utcnow, verbose_name='发布日期')
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='发布用户', db_column='userid')
    visits = models.IntegerField(default=0, verbose_name='浏览量')
    thumbup = models.IntegerField(default=0, verbose_name='点赞数')
    comment = models.IntegerField(default=0, verbose_name='回复数')
    # 一对多关联属性
    parent = models.ForeignKey("self", on_delete=models.CASCADE, verbose_name='上级ID')

    class Meta:
        db_table = 'tb_split'
        verbose_name = '吐槽表'
        verbose_name_plural = verbose_name

